# Deployment

This serves as a master repository to deploy everything else, 
just like the One Ring used to rule them all.

This will autodeploy to develop.

## nginx

The basic reverse proxy that also serves videos directly.

## deploy

The container that will periodically execute on the target environment 
in order to update the production. 

In order to install it, just launch [dronhub-update.sh](dronhub-update.sh).

It does such things as:

1. Download itself, so it can launch it's updated self when next cron time comes.
2. Pull all master images
3. Redeploy production
4. Prune unnecessary images
5. Make sure that it keeps being executed daily

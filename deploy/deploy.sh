#!/usr/bin/env bash

cp -f /dronhub/dronhub-update.sh /host/etc/cron.daily/dronhub-update
docker login -u "${CI_REGISTRY_USER}" -p "${CI_REGISTRY_PASSWORD}" git.cervirobotics.com:4567
docker pull git.cervirobotics.com:4567/dronhub/orlen-backend/deployment/deploy:master
docker-compose pull
docker image prune -f
docker-compose up -d
